from bs4 import BeautifulSoup
import requests
import pandas as pd
from pandas import ExcelWriter

# need to get a list of urls for each letter of the alphabet on this page
url = r'http://nlcba.org/professional-resources/nlcba-directory/'

html_content = requests.get(url).text

soup = BeautifulSoup(html_content, 'lxml')

names = []
email_addresses = []
phone_numbers = []

for entry in soup.find_all(class_='cn-entry'):
    # find names
    given_name = entry.find(class_='given-name')
    if given_name:
        given_name = given_name.get_text()
    else:
        break

    family_name = entry.find(class_='family-name')
    if family_name:
        family_name = family_name.get_text()
    else:
        family_name = ''

    name = given_name+' '+family_name.lower()
    name = name.title()
    names.append(name)

    # find the email address
    email_address = entry.find(class_='email-address')
    if email_address:
        email_address = email_address.get_text().lower()
        email_addresses.append(email_address)
    else:
        email_addresses.append('Nan')

    # find the phone number
    phone_number = entry.find(class_='tel cn-phone-number')
    if phone_number:
        phone_number = phone_number.get_text()
        phone_number = phone_number.replace('work', '')
        phone_number = phone_number.replace('fax', '')
        phone_number = phone_number.replace('home', '')
        phone_number = phone_number.replace('/', '-')
        phone_numbers.append(phone_number)
    else:
        phone_numbers.append('Nan')

# arrange into dataframe for .csv export
data = pd.DataFrame({'Name': names,
                     'Email Address': email_addresses,
                     'Phone': phone_numbers})
emails_only_df = data.loc[data['Email Address'] != 'Nan']
phones_only_df = data.loc[(data['Email Address'] == 'Nan') & (data['Phone'] != 'Nan')]
empty_contacts_df = data.loc[(data['Phone'] == 'Nan') & (data['Email Address'] == 'Nan')]

writer = ExcelWriter('NLCBA_directory.xlsx')
emails_only_df.to_excel(writer, sheet_name='Contacts w Email and Phone')
phones_only_df.to_excel(writer, sheet_name='Contacts w Phones Only')
empty_contacts_df.to_excel(writer, sheet_name='Empty Contacts')
writer.save()

